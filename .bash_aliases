# divio cloud
alias sshagent='eval $(ssh-agent -s)'
alias sshadddivio='ssh-add ~/.ssh/manjaro-divio_rsa'
alias sshaddgithub='ssh-add ~/.ssh/manjaro-github_rsa'

# linux commands
alias ownset='sudo chown -R $USER:$USER'
alias co='sudo chwon -R'
